package com.example.barikrecyclerview

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bintrecycleview.model.UserData
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var button: FloatingActionButton
    private lateinit var recv: RecyclerView
    private lateinit var userList: ArrayList<UserData>
    private lateinit var userAdapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userList = ArrayList()
        recv = findViewById(R.id.Recycler)
        button = findViewById(R.id.addButton)

        userAdapter = UserAdapter(this, userList)
        recv.layoutManager = LinearLayoutManager(this)
        recv.adapter = userAdapter

        button.setOnClickListener { showCustomAlertDialog() }
    }

    private fun showCustomAlertDialog() {
        val dialogView = layoutInflater.inflate(R.layout.add_item, null)
        val userName = dialogView.findViewById<EditText>(R.id.Nama)
        val nim = dialogView.findViewById<EditText>(R.id.Nim)

        AlertDialog.Builder(this).apply {
            setView(dialogView)

            val positiveText = "Ya"
            val negativeText = "Tidak"

            val spannablePositive = SpannableString(positiveText)
            val spannableNegative = SpannableString(negativeText)

            val positiveColor = ContextCompat.getColor(this@MainActivity, R.color.blue)
            val negativeColor = ContextCompat.getColor(this@MainActivity, R.color.blue)

            spannablePositive.setSpan(
                ForegroundColorSpan(positiveColor),
                0,
                positiveText.length,
                0
            )

            spannableNegative.setSpan(
                ForegroundColorSpan(negativeColor),
                0,
                negativeText.length,
                0
            )

            setPositiveButton(spannablePositive) { dialog, _ ->
                val name = userName.text.toString()
                val nimText = nim.text.toString()

                userList.add(UserData("Nama: $name", "Nim: $nimText"))
                userAdapter.notifyDataSetChanged()
                dialog.dismiss()
            }

            setNegativeButton(spannableNegative) { dialog, _ ->
                dialog.dismiss()
            }

            create().show()
        }
    }
}
