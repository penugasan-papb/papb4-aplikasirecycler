package com.example.barikrecyclerview

import android.app.AlertDialog
import android.content.Context
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.bintrecycleview.model.UserData


class UserAdapter(
    private val context: Context,
    private val userList: ArrayList<UserData>
) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    inner class UserViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name = view.findViewById<TextView>(R.id.Name)
        val nim = view.findViewById<TextView>(R.id.Nim)
        val deleteIcon = view.findViewById<ImageView>(R.id.DeleteIcon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.list_item, parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = userList[position]
        holder.name.text = user.userName
        holder.nim.text = user.nim

        holder.deleteIcon.setOnClickListener {
            showDeleteConfirmationDialog(position)
        }
    }

    private fun showDeleteConfirmationDialog(position: Int) {
        AlertDialog.Builder(context).apply {
            setTitle("Konfirmasi Hapus")
            setMessage("Apakah Anda yakin ingin menghapus item ini?")

            val positiveText = "Ya"
            val negativeText = "Tidak"

            val spannablePositive = SpannableString(positiveText)
            val spannableNegative = SpannableString(negativeText)

            val positiveColor = ContextCompat.getColor(context, R.color.blue)
            val negativeColor = ContextCompat.getColor(context, R.color.blue)

            spannablePositive.setSpan(
                ForegroundColorSpan(positiveColor),
                0,
                positiveText.length,
                0
            )

            spannableNegative.setSpan(
                ForegroundColorSpan(negativeColor),
                0,
                negativeText.length,
                0
            )

            setPositiveButton(spannablePositive) { _, _ ->
                userList.removeAt(position)
                notifyDataSetChanged()
                updateEmptyViewVisibility()
            }

            setNegativeButton(spannableNegative) { dialog, _ ->
                dialog.dismiss()
                updateEmptyViewVisibility()
            }

            show()
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun updateEmptyViewVisibility() {

    }
}

